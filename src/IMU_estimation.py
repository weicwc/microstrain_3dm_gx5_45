#!/usr/bin/env python

## State estimation for the robot
import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'library'))
sys.dont_write_bytecode = True

import time
import warnings
import numpy as np

## Personal libraries
from constant import *
import control_interface 								# action selection from ROS parameter server
import robot_class 										# class for robot states and function

## ROS messages
import rospy
from sensor_msgs.msg import Imu 						# sub msg for IMU
from sensor_msgs.msg import JointState 					# sub msg for joint states
from trajectory_msgs.msg import JointTrajectoryPoint	# pub msg for Robotis joints
from std_msgs.msg import Float64 						# pub msg for Gazebo joints
from std_msgs.msg import ByteMultiArray 				# foot contact state
from std_msgs.msg import Float32MultiArray				# foot contact force

## Robotis ROS msgs for joint control
from robotis_controller_msgs.msg import SyncWriteItem 	# pub msg for Robotis joints
from robotis_controller_msgs.msg import SyncWriteMulti 	# pub msg for Robotis joints

class CorinState:
	def __init__(self):
		rospy.init_node('IMU_estimator') 		#Initialises node

		self.interval = TRAC_INTERVAL
		self.rate 	  = rospy.Rate(1.0/TRAC_INTERVAL)	# frequency
		self.reset = True
		self.t = 0
		self.T = 0.01

		self.pub_x = rospy.Publisher('imu_x', Float64, queue_size=1)
		self.pub_y = rospy.Publisher('imu_y', Float64, queue_size=1)
		self.pub_z = rospy.Publisher('imu_z', Float64, queue_size=1)

		self.IMU_sub_  = rospy.Subscriber('/imu/data', Imu, self.Imu_callback, queue_size=5)

		self.v = np.array([0.0, 0.0, 0.0])
		self.x = np.array([0.0, 0.0, 0.0])
		self.g_vec = [0.0] * 1000

		rospy.sleep(0.5)

		np.set_printoptions(suppress=True) # suppress scientific notation
		np.set_printoptions(precision=5) # number display precision


	def Imu_callback(self, msg): 		# robot joint state callback

		a = msg.linear_acceleration.x
		t = msg.header.stamp.to_sec()

		#self.v += a * self.T
		#self.x += self.v * self.T

		#self.pub_.publish(self.x)

		if self.reset:
			self.t0 = t
			self.samples = 0
			self.reset = False
			self.mag_sum = 0
		else:
			self.samples += 1
			#print("samples", self.samples)
			#print((t-self.t0)/self.samples)

		o = msg.orientation
		orientation = np.array([o.w, o.x, o.y, o.z])
		euler = tf.euler_from_quaternion(orientation, 'rxyz')
		euler_deg = tuple([k * 180/np.pi for k in euler])

		#print(euler_deg)

		acc = np.array([msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z])
		g_mag = np.linalg.norm(acc)

		self.g_vec.pop()
		self.g_vec.insert(0,g_mag)

		self.mag_sum += g_mag
		#mag_avg = self.mag_sum/self.samples
		mag_avg = np.mean(self.g_vec)
		print("g_avg", mag_avg)
		print("g", g_mag)

		#print("mag", g_mag)

		#print(t-self.t)
		#self.t = t

		#gravity = g_mag * np.array([0, 0, 1, 0])
		gravity = 9.81 * np.array([0, 0, 1, 0])

		#g = qv_mult(tf.quaternion_inverse(orientation), gravity)
		#g = qv_mult(orientation, gravity)

		#print(g)

 		R = tf.quaternion_matrix(orientation)

		# estimated gravity
		v = np.dot(np.linalg.inv(R), gravity.transpose())

		# corrected accelerometer data
		acc2 = acc-v[:3]

		'''self.pub_x.publish(acc2[0])
		self.pub_y.publish(acc2[1])
		self.pub_z.publish(acc2[2])'''

		if (self.samples % 400) == 0:
			self.v = np.array([0.0, 0.0, 0.0])
			self.x = np.array([0.0, 0.0, 0.0])
		else:
			'''
			thresh = 1
			if abs(acc2[0]) < thresh:
				acc2[0] = 0
			if abs(acc2[1]) < thresh:
				acc2[1] = 0
			if abs(acc2[2]) < thresh:
				acc2[2] = 0

			if abs(acc2[0]) < thresh:
				self.v = np.array([0.0, 0.0, 0.0])
			else:
				self.v += acc2 * self.T'''
			self.v += acc2 * self.T
			#self.x += self.v * self.T
			self.x += (self.v * self.T) + (0.5 * acc2 * self.T * self.T)

		#self.pub_x.publish(mag_avg)
		#self.pub_y.publish(g_mag)
		self.pub_x.publish(self.x[0])
		self.pub_y.publish(self.x[1])
		self.pub_z.publish(self.x[2])

		#print(v)

# rotate vector v1 by quaternion q1
def qv_mult(q1, v1):
    #v1 = tf.unit_vector(v1)
    q2 = list(v1)
    q2.append(0.0)
    return tf.quaternion_multiply(
        tf.quaternion_multiply(q1, q2),
        tf.quaternion_conjugate(q1)
    )#[1:4]


if __name__ == "__main__":

	state = CorinState()

	rospy.spin()
